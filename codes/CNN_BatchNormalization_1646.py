import numpy as np
import pandas as pd
import keras as k
from keras.layers import Merge
from keras.layers.normalization import BatchNormalization
from keras.callbacks import ModelCheckpoint,EarlyStopping,ReduceLROnPlateau
from keras.callbacks import History
from keras.layers import Activation
from keras.models import model_from_json
from keras.optimizers import Adam
from matplotlib import pyplot as plt
from scipy.ndimage import rotate as rot

from sklearn.model_selection import StratifiedKFold, StratifiedShuffleSplit
from keras.preprocessing.image import ImageDataGenerator
from sklearn.metrics import log_loss
import cv2



import argparse
parser = argparse.ArgumentParser()
parser.add_argument("--model_option", help="Select model type as cnn/vgg16/xception", type=str, default='CNN')
parser.add_argument("--resize", help="Set resized image size", type=int, default=75)
args = parser.parse_args()
model_option = args.model_option 
image_resize = args.resize

resize_H, resize_W = image_resize, image_resize

file_path = '../input/train.json'

train = pd.read_json(file_path)

print(train.head())
train.shape

train[train['inc_angle'] == 'na'].count()

train.inc_angle = train.inc_angle.map(lambda x: 0.0 if x == 'na' else x)

def transform (df):
    images = []
    for i, row in df.iterrows():
        band_1 = np.array(row['band_1']).reshape(75,75)
        band_2 = np.array(row['band_2']).reshape(75,75)
        band_3 = (band_1 + band_2)/2.0
        
        band_1_norm = (band_1 - band_1.mean()) / (band_1.max() - band_1.min())
        band_2_norm = (band_2 - band_2. mean()) / (band_2.max() - band_2.min())
        band_3_norm = (band_3 - band_3.mean()) / (band_3.max() - band_3.min())
        
        images.append(np.dstack((band_1_norm, band_2_norm, band_3_norm)))
    
    return np.array(images)

def augment(images):
    image_mirror_lr = []
    image_mirror_ud = []
    image_rotate = []
    image_original = []
    for i in range(0,images.shape[0]):
        band_1 = images[i,:,:,0]
        band_2 = images[i,:,:,1]
        band_3 = images[i,:,:,2]        
        tmp = np.dstack((band_1, band_2, band_3)) 

        if model_option == 'CNN':
            image_original.append(tmp)
        else:            
            image_original.append(cv2.resize(tmp, (resize_W,resize_H), interpolation = cv2.INTER_CUBIC))   
               
        """
        # mirror left-right
        band_1_mirror_lr = np.flip(band_1, 0)
        band_2_mirror_lr = np.flip(band_2, 0)
        band_3_mirror_lr = np.flip(band_3, 0)
        tmp = np.dstack((band_1_mirror_lr, band_2_mirror_lr, band_3_mirror_lr))

        if model_option == 'CNN':
            image_mirror_lr.append(tmp)
        else:
            image_mirror_lr.append(cv2.resize(tmp, (resize_W,resize_H), interpolation = cv2.INTER_CUBIC))  

       
        # mirror up-down
        band_1_mirror_ud = np.flip(band_1, 1)
        band_2_mirror_ud = np.flip(band_2, 1)
        band_3_mirror_ud = np.flip(band_3, 1)
        tmp = np.dstack((band_1_mirror_ud, band_2_mirror_ud, band_3_mirror_ud))

        if model_option == 'CNN':
            image_mirror_ud.append(tmp)
        else: 
            image_mirror_ud.append(cv2.resize(tmp, (resize_W,resize_H), interpolation = cv2.INTER_CUBIC)) 
        
        #rotate 
        band_1_rotate = rot(band_1, 30, reshape=False)
        band_2_rotate = rot(band_2, 30, reshape=False)
        band_3_rotate = rot(band_3, 30, reshape=False)
        tmp = np.dstack((band_1_rotate, band_2_rotate, band_3_rotate))

        if model_option == 'CNN':
            image_rotate.append(tmp)
        else:
            image_rotate.append(cv2.resize(tmp, (resize_W,resize_H), interpolation = cv2.INTER_CUBIC))
        """
    original = np.array(image_original)
    """
    mirrorlr = np.array(image_mirror_lr)
    mirrorud = np.array(image_mirror_ud)
    rotated = np.array(image_rotate)
    """


    #images = np.concatenate((original, mirrorlr, mirrorud, rotated))
    images = original
    return images

print(model_option)


train_X = transform(train)
train_y = np.array(train ['is_iceberg'])

indx_tr = np.where(train.inc_angle > 0)
print (indx_tr[0].shape)

train_y = train_y[indx_tr[0]]
train_X = train_X[indx_tr[0], ...]

train_X = augment(train_X)
train_y = train_y #np.concatenate((train_y,train_y, train_y, train_y))

print (train_X.shape)
print (train_y.shape)


test_file = '../input/test.json'
test = pd.read_json(test_file)
test.inc_angle = test.inc_angle.replace('na',0)
test_X = transform(test)
test_X = augment(test_X)
print (test_X.shape)





def CNN_Model():
    model = k.models.Sequential()

    model.add(k.layers.convolutional.Conv2D(64, kernel_size=(3,3), input_shape=(75,75,3)))
    model.add(Activation('relu'))
    model.add(BatchNormalization())
    model.add(k.layers.convolutional.MaxPooling2D(pool_size=(3,3), strides=(2,2)))
    model.add(k.layers.Dropout(0.2))

    model.add(k.layers.convolutional.Conv2D(128, kernel_size=(3, 3)))
    model.add(Activation('relu'))
    model.add(BatchNormalization())
    model.add(k.layers.convolutional.MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))
    model.add(k.layers.Dropout(0.2))

    model.add(k.layers.convolutional.Conv2D(128, kernel_size=(3, 3)))
    model.add(Activation('relu'))
    model.add(BatchNormalization())
    model.add(k.layers.convolutional.MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))
    model.add(k.layers.Dropout(0.3))

    model.add(k.layers.convolutional.Conv2D(64, kernel_size=(3, 3)))
    model.add(Activation('relu'))
    model.add(BatchNormalization())
    model.add(k.layers.convolutional.MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))
    model.add(k.layers.Dropout(0.3))

    model.add(k.layers.Flatten())

    model.add(k.layers.Dense(512))
    model.add(Activation('relu'))
    model.add(BatchNormalization())
    model.add(k.layers.Dropout(0.2))

    model.add(k.layers.Dense(256))
    model.add(Activation('relu'))
    model.add(BatchNormalization())
    model.add(k.layers.Dropout(0.2))


    model.add(k.layers.Dense(1))
    model.add(Activation('sigmoid'))


    model.summary()
    return model

from keras.engine.training import Model as KerasModel
from keras.applications.vgg16 import VGG16
def vgg16_model_v1():              

    base_model = VGG16(weights='imagenet', include_top=False, input_shape=(resize_H, resize_W, 3))
      
    x1 = base_model.output
    x = k.layers.Flatten()(x1)
    x = k.layers.Dense(512)(x)
    x = Activation('relu')(x)
    x = k.layers.Dropout(0.5)(x)
    x = k.layers.Dense(512,init='he_normal')(x)
    #x = BatchNormalization()(x)
    x = Activation('relu')(x)
    x = k.layers.Dropout(0.5)(x)
    out_me = k.layers.Dense(1, activation='sigmoid', name = 'out_m')(x)

    model = KerasModel(input=base_model.input, output=out_me)
 
    for layer in model.layers:
        layer.trainable = True 


    model.summary()

    return model

def vgg16_model():
    base_model = VGG16(weights='imagenet', include_top=False, input_shape=(resize_H, resize_W, 3), classes=1)
    x = base_model.get_layer('block5_pool').output
    x = k.layers.GlobalAveragePooling2D()(x)
    predictions = k.layers.Dense(1, activation='sigmoid',kernel_initializer='he_normal')(x)
    
    model = KerasModel(input=base_model.input, output=predictions)


    model.summary()
    return model


from keras.applications.xception import Xception
# create the base pre-trained model
def xception_model():
                    
    base_model = Xception(include_top=False,weights='imagenet',input_tensor=None, input_shape=(resize_H, resize_W, 3))

    x = base_model.get_layer('block13_sepconv2_bn').output#base_model.output
    
    x = k.layers.GlobalAveragePooling2D()(x)
    predictions = k.layers.Dense(1, activation='sigmoid')(x)

    # this is the model we will train
    model = KerasModel(input=base_model.input, output=predictions)
    for layer in model.layers:
        layer.trainable = True 


    model.summary()
    return model



from keras.applications.resnet50 import ResNet50
# create the base pre-trained model
def resnet50_model():
                    
    base_model = ResNet50(include_top=False,weights='imagenet',input_tensor=None, input_shape=(resize_H, resize_W, 3))

    x = base_model.get_layer('activation_49').output#base_model.output
    
    x = k.layers.GlobalAveragePooling2D()(x)

    x = k.layers.Dropout(0.5)(x)
    predictions = k.layers.Dense(1, activation='sigmoid')(x)


    # this is the model we will train
    model = KerasModel(input=base_model.input, output=predictions)
    for layer in model.layers:
        layer.trainable = True 



    model.summary()
    return model



from keras.applications.mobilenet import MobileNet
# create the base pre-trained model
def mobilenet_model():
                    
    base_model = MobileNet(include_top=False,weights='imagenet',input_tensor=None, input_shape=(resize_H, resize_W, 3))

    x = base_model.output
    x = k.layers.GlobalAveragePooling2D()(x)
    x = k.layers.Dropout(0.5)(x)
    predictions = k.layers.Dense(1, activation='sigmoid')(x)


    # this is the model we will train
    model = KerasModel(input=base_model.input, output=predictions)
    for layer in model.layers:
        layer.trainable = True 



    model.summary()
    return model



def train():

    
    if model_option == 'vgg16':
        model = vgg16_model()
    elif model_option =='xception':
        model = xception_model()
    elif model_option == 'resnet50':
        model = resnet50_model()
    elif model_option == 'mobilenet':
        model = mobilenet_model()
    else:
        model = CNN_Model()

    learning_method = Adam(lr=0.001) #SGD(lr=1e-3, decay=1e-6, momentum=0.9, nesterov=True) #Adam(lr=LR)
    model.compile(optimizer=learning_method, loss='binary_crossentropy',metrics=["accuracy"])



    print ('training set:')
    print (train_X.shape)
    print (train_y.shape)

    batch_size = 32
    early_stopping = EarlyStopping(monitor = 'val_loss', patience = 10, verbose = 0, mode= 'min')
    reduce_lr_loss = ReduceLROnPlateau(monitor='val_loss', factor = 0.1, patience = 5, verbose =1, 
                                   epsilon = 1e-4, mode='min', min_lr = 0.0001)
    model_filepath='./weights.{:}.hdf5'.format(model_option)
    checkpoint = ModelCheckpoint(model_filepath, monitor='val_loss', verbose=1, save_best_only=True, mode='min')
    callbacks_list = [early_stopping, checkpoint]

    history = model.fit(train_X, train_y, batch_size = batch_size, epochs =30, verbose =1, validation_split = 0.15, 
              callbacks=callbacks_list)

    print (history.history.keys())
    fig = plt.figure()
    plt.plot(history.history['acc'])
    plt.plot(history.history['val_acc'])
    plt.title('model accuracy')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train','test'],loc='upper left')
    plt.show()


    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper right')
    plt.show()

    model_json = model.to_json()
    with open("./{:s}_model.json".format(model_option), "w") as json_file:
        json_file.write(model_json)

    # load json and create model
    json_file = open('./{:s}_model.json'.format(model_option), 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model = model_from_json(loaded_model_json)
    # load weights into new model
    loaded_model.load_weights('./weights.{:s}.hdf5'.format(model_option))
    print("Loaded model from disk")
    loaded_model.compile(loss='binary_crossentropy', optimizer = Adam(lr=0.001), metrics=['accuracy'])


    pred_test = loaded_model.predict(test_X, verbose=1)
    submission = pd.DataFrame({'id': test["id"], 'is_iceberg': pred_test.reshape((pred_test.shape[0]))})
    submission.to_csv('./DNN_AUG_submission.csv', index=False)

#train()



#Using K-fold Cross Validation with Data Augmentation.

def myAngleCV(X_train, y_train, X_test, max_iter):
    for iter in range(max_iter):  
        preds = myAngleCV_iter(X_train, y_train, X_test, iter)

        #Submission for each day.
        submission = pd.DataFrame()
        submission['id']=test['id']
        submission['is_iceberg']=preds
        submission.to_csv('{:}_model_{:d}.csv'.format(model_option, iter), index=False)

def myAngleCV_iter(X_train, y_train, X_test, iter):
    K=5
    folds = list(StratifiedKFold(n_splits=K, shuffle=True, random_state=16+iter).split(X_train, y_train))
    y_test_pred_log = 0
    y_train_pred_log=0
    y_valid_pred_log = 0.0*y_train
    for j, (train_idx, test_idx) in enumerate(folds):
        print('\n===================FOLD=',j)
        X_train_cv = X_train[train_idx]
        y_train_cv = y_train[train_idx]
        X_holdout = X_train[test_idx]
        Y_holdout= y_train[test_idx]
        print(X_train_cv.shape, y_train_cv.shape, X_holdout.shape, Y_holdout.shape)

        if model_option == 'vgg16':
            galaxyModel= vgg16_model()
        elif model_option =='xception':
            galaxyModel= xception_model()
        elif model_option == 'resnet50':
            galaxyModel= resnet50_model()
        elif model_option == 'mobilenet':
            galaxyModel= mobilenet_model()
        else:
            galaxyModel= CNN_Model()

        learning_method = Adam(lr=0.0001) #SGD(lr=1e-3, decay=1e-6, momentum=0.9, nesterov=True) #Adam(lr=LR)
        galaxyModel.compile(optimizer=learning_method, loss='binary_crossentropy',metrics=["accuracy"])


        batch_size = 32
        early_stopping = EarlyStopping(monitor = 'val_loss', patience = 20, verbose = 0, mode= 'min')
        reduce_lr_loss = ReduceLROnPlateau(monitor='val_loss', factor = 0.1, patience = 10, verbose =1, 
                                   epsilon = 1e-4, mode='min', min_lr = 0.0001)
        file_path='./{:}_weights.{:}.hdf5'.format(j, model_option)
        checkpoint = ModelCheckpoint(file_path, monitor='val_loss', verbose=1, save_best_only=True, mode='min')
        callbacks_list = [early_stopping, checkpoint]

        #history = galaxyModel.fit(X_train_cv, y_train_cv, batch_size = batch_size, epochs =30, verbose =1, validation_data=(X_holdout, Y_holdout), callbacks=callbacks_list)
       
        datagen = ImageDataGenerator(
                shear_range=0.1,
                zoom_range=0.1,
                rotation_range=10.0,
                width_shift_range=0.1,
                height_shift_range=0.1,
                vertical_flip = True,
                horizontal_flip = True)

        galaxyModel.fit_generator(
                datagen.flow(X_train_cv, y_train_cv, batch_size=batch_size),
                steps_per_epoch=len(y_train_cv)/batch_size,
                epochs=100,
                shuffle=True,
                verbose=1,
                validation_data=(X_holdout, Y_holdout),
                callbacks=callbacks_list)

        




        #Getting the Best Model
        galaxyModel.load_weights(filepath=file_path)
        #Getting Training Score
        score = galaxyModel.evaluate(X_train_cv, y_train_cv, verbose=0)
        print('Train loss:', score[0])
        print('Train accuracy:', score[1])
        #Getting Test Score
        score = galaxyModel.evaluate(X_holdout, Y_holdout, verbose=0)
        print('Test loss:', score[0])
        print('Test accuracy:', score[1])

        #Getting validation Score.
        pred_valid=galaxyModel.predict(X_holdout)
        y_valid_pred_log[test_idx] = pred_valid.reshape(pred_valid.shape[0])

        #Getting Test Scores
        temp_test=galaxyModel.predict(X_test)
        y_test_pred_log+=temp_test.reshape(temp_test.shape[0])

        #Getting Train Scores
        temp_train=galaxyModel.predict(X_train)
        y_train_pred_log+=temp_train.reshape(temp_train.shape[0])

    y_test_pred_log=y_test_pred_log/K
    y_train_pred_log=y_train_pred_log/K

    print('\n Train Log Loss Validation= ',log_loss(y_train, y_train_pred_log), 'iter= ', iter)
    print(' Test Log Loss Validation= ',log_loss(y_train, y_valid_pred_log), 'iter= ', iter)
    return y_test_pred_log
print('training data:')
print(train_X.shape, train_y.shape, test_X.shape)
myAngleCV(train_X, train_y, test_X, 1)







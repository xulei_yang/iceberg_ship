

import os, fnmatch
from os.path import basename

import pandas as pd
import numpy as np

FILE_PATH = "../binh7"
arrlist = [os.path.join(dirpath, f)
    for dirpath, dirnames, files in os.walk(FILE_PATH)
    for f in fnmatch.filter(files, '*.csv')]

print(arrlist)

bSort = True
data = pd.read_csv(arrlist[0])
if bSort:
    init_names = data['id'] 
    idx = init_names.argsort()
    names = init_names[idx]
else:
    names = data['id']
num = len(names)


arrValue = np.zeros((num,1),np.float32)


arrV = []

for ls in arrlist:
    data = pd.read_csv(ls)
    tmp = np.array(data['is_iceberg'])  
    cur_names = data['id'] 
    #import pdb; pdb.set_trace()  #idx = cur_names.index(names)
    if bSort:
        idx = cur_names.argsort()      
        arrV.append(tmp[idx])
    else:
        arrV.append(tmp)


arrV = np.array(arrV)


print('printing the correlation')
for k1 in range(len(arrlist)):
    for k2 in range(k1+1, len(arrlist)):
        corr = np.corrcoef(arrV[k1], arrV[k2])
        if corr[0][1] < 1.0 and corr[0][1] > 0.5:
            print(arrlist[k1][-20:], arrlist[k2][-20:], corr[0][1])
    

import pdb; pdb.set_trace()

"""
iThd = 5
for k in range(num):
    tmpV = arrV[:,k]
    idx_low = np.where(tmpV<0.1)[0]    
    idx_high = np.where(tmpV>0.9)[0]
    idx_low_mid = np.where((tmpV>0.1) & (tmpV<0.5))[0]
    idx_mid_high = np.where((tmpV>0.5) & (tmpV<0.9))[0]
    len_low = len(idx_low)
    len_high = len(idx_high)
    len_low_mid = len(idx_low_mid)
    len_mid_high = len(idx_mid_high)
    if len_low > iThd :
        value = np.min(tmpV[idx_low])
    elif len_high > iThd : 
        value = np.max(tmpV[idx_high])
    elif len_low_mid > iThd :
        value = np.min(tmpV[idx_low_mid])
    elif len_mid_high > iThd :
        value = np.max(tmpV[idx_mid_high])
    else:
        value = np.median(tmpV)
    arrValue[k] = value


iThd1 = 4
iThd2 = 5

for k in range(num):
    tmpV = arrV[:,k]
    idx_low = np.where(tmpV<0.1)[0]    
    idx_high = np.where(tmpV>0.9)[0]
    idx_low_mid = np.where((tmpV>0.1) & (tmpV<0.5))[0]
    idx_mid_high = np.where((tmpV>0.5) & (tmpV<0.9))[0]
    idx_half_low = np.where(tmpV<0.5)[0]
    idx_half_high = np.where(tmpV>0.5)[0]
 
    len_low = len(idx_low)
    len_high = len(idx_high)
    len_low_mid = len(idx_low_mid)
    len_mid_high = len(idx_mid_high)
    len_half_low = len(idx_half_low)
    len_half_high = len(idx_half_high)


    if len_half_low > iThd1:
        if len_low > iThd1 :
            value = np.min(tmpV[idx_low])
        elif len_low_mid > iThd2 :
            value = np.min(tmpV[idx_low_mid])
        elif len_half_low > iThd2 :
            value = np.median(tmpV[idx_half_low])
        else:
            value = np.median(tmpV)

    elif len_half_high > iThd1:  
        if len_high > iThd1 : 
            value = np.max(tmpV[idx_high])
        elif len_mid_high > iThd2 :
            value = np.max(tmpV[idx_mid_high])
        elif len_half_high > iThd2 :
            value = np.median(tmpV[idx_half_high])
        else:
            value = np.median(tmpV)
    else:
        value = np.median(tmpV)
    arrValue[k] = value
"""

Thd4 = 4
Thd5 = 5
Thd6 = 6

for k in range(num):
    tmpV = arrV[:,k]
    idx_l10 = np.where(tmpV<0.1)[0]
    idx_l30 = np.where(tmpV<0.3)[0]
    idx_l50 = np.where(tmpV<0.5)[0]
    idx_l1030 = np.where(tmpV>0.1) & np.where(tmpV<0.3)[0]
    idx_l3050 = np.where(tmpV>0.3) & np.where(tmpV<0.5)[0]

    idx_h90 = np.where(tmpV>0.9)[0]
    idx_h70 = np.where(tmpV>0.7)[0]
    idx_h50 = np.where(tmpV>0.5)[0]
    idx_h7090 = np.where(tmpV>0.7) & np.where(tmpV<0.9)[0]
    idx_h5070 = np.where(tmpV>0.5) & np.where(tmpV<0.7)[0]

    idx_mix = np.where(tmpV>0.3) & np.where(tmpV<0.7)[0]
    

    len_l10 = len(idx_l10)
    len_l30 = len(idx_l30)
    len_l50 = len(idx_l50)
    len_l1030 = len(idx_l1030)
    len_l3050 = len(idx_l3050)

    len_h90 = len(idx_h90)
    len_h70 = len(idx_h70)
    len_h50 = len(idx_h50)
    len_h7090 = len(idx_h7090)
    len_h5070 = len(idx_h5070)

    len_mix = len(idx_mix)

       
    if len_l50 > Thd4: #low > high 
        if len_l10 > Thd5:
            value = np.min(tmpV[idx_l10])
        elif len_l1030 > Thd5:
            value = np.min(tmpV[idx_l1030])
        elif len_l3050 > Thd5:
            value = np.min(tmpV[idx_l3050])
        elif len_l30 > Thd5:
            value = np.median(tmpV[idx_l30])
        elif len_l50 > Thd5:
            value = np.median(tmpV[idx_l50])
        elif len_mix > Thd5:
            value = np.median(tmpV[idx_mix])
        else: 
            value = np.median(tmpV)
  
    elif len_h50 > Thd4: # high > low
        if len_h90 > Thd5:
            value = np.min(tmpV[idx_h90])
        elif len_h7090 > Thd5:
            value = np.min(tmpV[idx_h7090])
        elif len_h5070 > Thd5:
            value = np.min(tmpV[idx_h5070])
        elif len_h70 > Thd5:
            value = np.median(tmpV[idx_h70])
        elif len_h50 > Thd5:
            value = np.median(tmpV[idx_h50])
        elif len_mix > Thd5:
            value = np.median(tmpV[idx_mix])
        else: 
            value = np.median(tmpV)
    else:
        print('else')
        value = np.median(tmpV)
    arrValue[k] = value

arrValue=arrValue.reshape(num)


test = pd.read_json("../input/test.json")
init_angle = test['inc_angle']
idx = init_names.argsort()
angle = init_angle[idx]

import pdb; pdb.set_trace()

val = 34.4721, 42.5591, 33.6352, 36.1061, 39.2340
for v in val:
    idx = np.where(angle==v)[0]
    print('angle = {:.8f}'.format(angle[idx[0]]))
    print(arrValue.shape, np.shape(angle))
    print(arrValue[idx])
    arrValue[idx] = 0.9999  #ice_berg





#import pdb; pdb.set_trace()

print("Generating submission file...")

output = pd.DataFrame({'id': names,
        'is_iceberg': arrValue})
# set col 'ParceID' to first col
cols = output.columns.tolist()
cols = cols[-1:] + cols[:-1]
output = output[cols]

print(len(names), len(arrlist))
output.to_csv('mean_results.csv', index=False,  float_format = '%.6f')





